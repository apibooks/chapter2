﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.IO;
using System.Net.Http;
using System.Collections;

namespace ConsoleApp1
{
    class Program
    {
        static async Task RunAPI()
        {
            string url = "http://cyberjapandata2.gsi.go.jp/general/dem/scripts/getelevation.php?lon=140.08531&lat=36.103543&outtype=JSON";
            HttpClient client = new HttpClient();
            HttpResponseMessage response = client.GetAsync(url).Result;
            string responseBody = await response.Content.ReadAsStringAsync();
            Console.WriteLine(responseBody);

        }
        static void Main(string[] args)
        {
            Task task = RunAPI();
            task.Wait();
            Console.ReadKey();
        }
    }
}
