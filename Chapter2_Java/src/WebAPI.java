import java.io.IOException;
import java.net.URISyntaxException;
import javax.swing.JOptionPane;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;

public class WebAPI {
	public static void main(String[] args) throws URISyntaxException, IOException {
		String uri = "http://cyberjapandata2.gsi.go.jp/general/dem/scripts/getelevation.php";
		RequestConfig requestConfig = RequestConfig.custom().build();
		HttpClientBuilder cbuilder = HttpClientBuilder.create();
		cbuilder = cbuilder.setDefaultRequestConfig(requestConfig);
		HttpClient httpclient = cbuilder.build();
		URIBuilder ubuilder = new URIBuilder(uri);
		ubuilder = ubuilder.addParameter("lon", "140.08531");
		ubuilder = ubuilder.addParameter("lat", "36.103543");
		ubuilder = ubuilder.addParameter("outtype", "JSON");
		HttpGet request = new HttpGet(ubuilder.build());
		HttpResponse response = httpclient.execute(request);
		HttpEntity entity = response.getEntity();
		String content = EntityUtils.toString(entity);
		JOptionPane.showMessageDialog(null, content);
	}
}
